import { TestBed, inject } from '@angular/core/testing';

import { FetchMovieService } from './fetch-movie.service';

describe('FetchMovieService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FetchMovieService]
    });
  });

  it('should be created', inject([FetchMovieService], (service: FetchMovieService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FetchMovieService {
  private APIKey = 'cf9401f5';
  private FullPath = '';
  private Movies = [];
  private TotalResultCount: Number;
  private CurrentPage = 0;
  private CurrentName = '';
  public DropCurrentMovies: boolean;


  constructor(private Http: HttpClient) {
    this.TotalResultCount = 0;
    this.DropCurrentMovies = false;
  }

  FetchMoviePlot(imdbId: string): Observable<Object> {
    const idpath = 'http://www.omdbapi.com/?apikey=' + this.APIKey + '&i=' + imdbId + '&plot=full';
    return this.Http.get(idpath);
  }

  FetchMovies(Title: string): Observable<Object> {
    Title = Title.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    if (Title !== this.CurrentName)
    {
      console.log('New title is :' + Title);
      this.CurrentName = Title;
      this.CurrentPage = 1;
      this.DropCurrentMovies = true;
    } else {
      this.CurrentPage++;
    }
      this.FullPath = 'http://www.omdbapi.com/?apikey=' + this.APIKey;
      this.FullPath = this.FullPath + '&s=' + this.CurrentName + '&page=' + this.CurrentPage.toString();
      console.log(this.FullPath);
      return this.Http.get(this.FullPath);
  }
}

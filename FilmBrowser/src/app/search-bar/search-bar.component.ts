import { Component, OnInit } from '@angular/core';
import { FetchMovieService } from '../FetchMovie/fetch-movie.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  MovieTitle = '';
  Movies = [];
  NewMovies = [];
  TotalResultCount = 0;
  IsAPIFiredNormally;
  IsLoading: boolean;
  FreshPage: boolean;
  NothingElseToFetch: boolean;
  NothingFound: boolean;
  CheckYourInternet: boolean;


  constructor(private fetchMovie: FetchMovieService) {
  }

  ngOnInit() {
    this.IsLoading = false;
    this.FreshPage = true;
    this.NothingFound = false;
    this.CheckYourInternet = false;
  }

  OnSearchClick () {
    this.FreshPage = false;
    this.NothingFound  = false;
    if (this.MovieTitle !== '') {
      console.log('On Search Click title is: ' + this.MovieTitle);
      this.IsLoading = true;
      this.fetchMovie.FetchMovies(this.MovieTitle).subscribe(
        data => {
          this.IsAPIFiredNormally = data['Response'];
          if (this.IsAPIFiredNormally === 'True') {
            this.NewMovies = data['Search'],
            this.TotalResultCount = data['totalResults'];
            console.log('Returned movies');
            console.log(this.NewMovies);
            if (this.TotalResultCount * 10 <= this.Movies.length) {
              this.NothingElseToFetch = true;
            } else {
              this.NothingElseToFetch = false;
            }
            if (this.fetchMovie.DropCurrentMovies === true) {
              this.fetchMovie.DropCurrentMovies = false;
              this.Movies = [];
            }
            for (let i = 0; i < this.NewMovies.length; i++ ) {
              this.fetchMovie.FetchMoviePlot(this.NewMovies[i].imdbID).subscribe(
                (dt) => {this.NewMovies[i].Plot = dt['Plot']; },
                (err) => {console.log('SomeKindOfErrorHappenedDuringHttpGet(MoviePlots) ' + err.toString()); this.CheckYourInternet = true; this.IsLoading = false; this.FreshPage = true;},
                () => {},
              );
              this.Movies.push(this.NewMovies[i]);
            }

          } else {
            this.NothingElseToFetch = true;
            if (data['Error'] === 'Movie not found!') {
              this.NothingFound = true;
            } else {
              console.log(data['Error']);
            }

          }
        },
      err => {console.log('SomeKindOfErrorHappenedDuringHttpGet ' + err.toString()); this.CheckYourInternet = true; this.IsLoading = false; this.FreshPage = true;},
      () => {this.IsLoading = false; console.log('All movies'); console.log(this.Movies); }
      );
      return false;
    } else {
      this.NothingFound = true;
      this.Movies = [];
    }
  }

  OnKeyPressed (event: any) {
    if (event.key === 'Enter') {
      this.OnSearchClick();
    }
  }
}


